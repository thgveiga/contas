package br.com.fiap.contas.modelo;

import br.com.fiap.contas.Conta;

public class Banco {

	private String nome;
	private int numero;
	private Conta[] contas;
	
	public Banco(String nome, int numero) {
		super();
		this.nome = nome;
		this.numero = numero;
		this.contas = new ContaCorrente[10];
	}

	public Conta[] getContas() {
		return contas;
	}

	public void setContas(Conta[] contas) {
		this.contas = contas;
	}

	public String getNome() {
		return nome;
	}

	public int getNumero() {
		return numero;
	}
	
	public void adiciona (Conta conta) {
	
		for(int i =0; i < contas.length; i++){
		   	if (contas[i] == null)
		   		contas[i] = conta;
		}
		
	}
	
	public void mostraContas() {
		for(Conta c : this.contas)
			System.out.println(c);
	}
	
	
	
}
